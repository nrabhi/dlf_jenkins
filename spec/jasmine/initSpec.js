const { until, By, Actions,Key } = require('selenium-webdriver');
const { buildDriver, closeDriver, selectItemOnSelectByXpath, clickWhenClickable } = require('../../factory');
const { run_time, } = require('../../config');
const { XPATH, SUBMIT, FILLINPUT, TEXT,FIELD, JS, RADIO_POPUP, FILE, SCROLL, SELECT, BUTTON, 
        NAVMENU, CHECK, OPENURL, TAG, URL, LINK, SCENARIO, WAIT, LOGOUT, TABLE, INNERTEXT,
        COLUMN,SEARCH_MENU_ITEM,IFRAME,TABLECOUNT,HOVERMENU, HOVER,CLICK, DOUBLECLICK, ROWS,
        VALUE,FILLRIGHTS, SWITCH, ONLYCHECK, COMPARE, CHECKINDEX, NOTHERE, CHECKNAME, CHECKFILE, EXPANDDIV, DOUBLE, ALERT, LOADING, EXECUTE, COMPARETABLE, COMPAREVALUE, CHECKVALUE, LENGTH, COMPAREVERSION
 } = require('../../constants');
const { loadjs } = require('../../buildxml');
const { getLocator } = require('../../locators');
const xml2js = require('xml2js');
const fs = require('fs');
const moment = require('moment')

let document;
const parser = new xml2js.Parser({
    explicitArray: false,
    mergeAttrs: true,
    explicitRoot: false,
    ignoreAttrs: false,
});

let numLastRow;
let nameSign;
let countFiles;
let countTr;
let savedText;
let savedNumberPage;
let printBloc = true;
let arrayofOpeartionObjectVersionFive = [];
let arrayofOpeartionObjectVersionFour = [];
let arrayofOpeartionObjectVersionFourThree = [];
let arrayofMenusObjectVersionFr= [];
let arrayofMenusObjectVersionEn = [];
let arrayOfDataVuesVersionFourTwo = [];
let arrayOfDataVuesVersionFourThree = [];
let arrayOfDataVuesVersionFive = [];
let arrayofEntityRightsVersionFive = [];
let arrayofEntityRightsVersionFour = [];

const filePathRights = './output/compare-rights.json';
const filePathEntity = './output/compare-rights-entity.json';
const filePathLangueVues = './output/compare-vues-langue.json';
const filePathDataVues = './output/compare-data-vues.json';


async function expectSpec(tc) {
    if (!tc.check) return;
    if(tc.check.iframe){
        await turnToIframe(tc.iframe);
    }
    switch (tc.check.type) {
        case TAG:
            try {
            const checkLocator = await getLocator(tc.check);
            await driver.wait(until.elementLocated(checkLocator), run_time.elementLocatedTimeOut);
            const checkElement = await driver.findElement(checkLocator);
            await expect(checkElement).toBeTruthy();
            break;
            } catch (error) {}
        case URL:
            const currentUrl = await driver.getCurrentUrl();
            let value = tc.check._;
            await expect(currentUrl).toEqual(`${value}`)
            break;
        case TABLECOUNT:
            const trLocator = await getLocator(tc.check);
            await driver.wait(until.elementsLocated(trLocator), run_time.elementLocatedTimeOut);
            const trElement = await driver.findElements(trLocator);
            await expect(trElement.length.toString()).toEqual(`${tc.check._}`);
            break;
        case NOTHERE:
            try{
                inputLocator = await getLocator(tc.check);
                inputElement = await driver.findElements(inputLocator);
                await expect(inputElement.length).toEqual(0);
                }catch(e){}
            break;
        case TABLE:
            for (let index in tc.check.expect) {
                let element = tc.check.expect[index];
                switch (element.type) {
                    case COMPARE:
                        inputLocator = await getLocator(element);
                        await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                        inputElement = await driver.findElement(inputLocator);
                        await inputElement.getText().then(async (res) => {
                        switch (element.operator) {
                            case ">":
                                await expect(parseInt(res)).toBeGreaterThan(parseInt(element._));
                                break;
                            case "<":
                                await expect(parseInt(res)).toBeLessThan(parseInt(element._));
                                break;
                            case "=":
                                await expect(parseInt(res)).toEqual(parseInt(element._));
                                break;
                        }
                        });
                        break;
                    case TAG:
                        const checkLocator = await getLocator(element);
                        await driver.wait(until.elementLocated(checkLocator), run_time.elementLocatedTimeOut);
                        const checkElement = await driver.findElement(checkLocator);
                        await expect(checkElement).toBeTruthy();
                        break;
                    case NOTHERE:
                        try{
                        inputLocator = await getLocator(element);
                        inputElement = await driver.findElements(inputLocator);
                        if (inputElement.length > 0) {
                            console.log(` *** This Element ${inputLocator.value} should Be Not Exist ***`)
                        }
                        await expect(inputElement.length).toEqual(0);
                        }catch(e){}
                    break;
                    case TABLECOUNT:
                        const trLocator = await getLocator(element);
                        await driver.wait(until.elementsLocated(trLocator), run_time.elementLocatedTimeOut);
                        const trElement = await driver.findElements(trLocator);
                        await expect(trElement.length.toString()).toEqual(`${element._}`);
                        break;
                    case CHECKVALUE:
                        const checkValueLocator = await getLocator(element);
                        await driver.wait(until.elementLocated(checkValueLocator), run_time.elementLocatedTimeOut);
                        const checkValueElement = await driver.findElement(checkValueLocator);
                        await checkValueElement.getText().then(async (res) => {
                            await expect(res).toEqual(`${element._}`);
                            });
                        break;
                }
            }
            break;
        case ROWS:
            for (let index in tc.check.row) {
                let element = tc.check.row[index];
                inputLocator = await getLocator(element);
                await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                const inputElement = await driver.findElement(inputLocator);
                await expect(inputElement).toBeTruthy();
            }
            break;
        case CHECKINDEX :
            inputLocator = await getLocator(tc.check);
            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
            inputElement = await driver.findElement(inputLocator);
            if (tc.check.del){
                await inputElement.getText().then(async (res) => {
                    const firstChar = res.charAt(0);
                    if(isLetter(firstChar)) {
                       res = res.substring(1);
                    }
                await expect(+res).toBeLessThan(+numLastRow);
                });
                break;
            }
            await inputElement.getText().then(async (res) => {
                const firstChar = res.charAt(0);
                if(isLetter(firstChar)) {
                   res = res.substring(1);
                }
            await expect(+res).toBeGreaterThan(+numLastRow);
            });
            break;
        case CHECKNAME :
                inputLocator = await getLocator(tc.check);
                await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                inputElement = await driver.findElement(inputLocator);
                await inputElement.getText().then(async (res) => {
                await expect(res).toContain(nameSign);
                });
            break;
        case CHECKFILE :
            checkFileExists(tc.check._);
            break;
        case COMPARE:
            if (tc.check.multi) {
                const valueLocator = await getLocator(tc.check);
                await driver.wait(until.elementsLocated(valueLocator), run_time.elementLocatedTimeOut);
                const valueElement = await driver.findElements(valueLocator);
                switch (tc.check.operator) {
                    case "greater":
                        await expect(valueElement.length.toString()).toBeGreaterThan(tc.check._);
                        break;
                    case "less":
                        await expect(valueElement.length.toString()).toBeLessThan(tc.check._);
                        break;
                    case "equal":
                        await expect(valueElement.length.toString()).toEqual(tc.check._);
                        break;
            }}else {
                inputLocator = await getLocator(tc.check);
                await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                inputElement = await driver.findElement(inputLocator);
                await inputElement.getText().then(async (res) => {
                switch (tc.check.operator) {
                        case ">":
                            await expect(parseInt(res)).toBeGreaterThan(parseInt(tc.check._));
                            break;
                        case "<":
                            await expect(parseInt(res)).toBeLessThan(parseInt(tc.check._));
                            break;
                        case "=":
                            await expect(parseInt(res)).toEqual(parseInt(tc.check._));
                            break;
                    }
                    });
                    break;
            }
            
        case COMPAREVALUE:
            const valueLocator = await getLocator(tc.check);
            await driver.wait(until.elementsLocated(valueLocator), run_time.elementLocatedTimeOut);
            const valueElement = await driver.findElements(valueLocator);
            switch (tc.check.operator) {
                case ">":
                    await expect(valueElement.length.toString()).toBeGreaterThan(countTr);
                    break;
                case "<":
                    await expect(valueElement.length.toString()).toBeLessThan(countTr);
                    break;
                case "=":
                    await expect(valueElement.length.toString()).toEqual(countTr);
                    break;
            }
    }
    if(tc.check.iframe) await resetToDefault();
}
async function expectSpecParam(actualOrder,expectedOrder){
    if (actualOrder === expectedOrder) {
        await expect(true).toBeTruthy();
    } else {
        await expect(false).toBeTruthy();
    }
}
function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
}
async function waitUntilLoaderDisappear(loading) {
    const loadingLocator = await getLocator(loading);
   try
   {
    const load = await driver.findElement(loadingLocator);
    await driver.wait(until.elementIsNotVisible(load), run_time.elementIsNotVisibleTimeOut);
   }catch(e)
   {

   }
}
async function turnToIframe(iframe) {
    const frameLocator = await getLocator(iframe);
    await driver.switchTo().frame(driver.findElement(frameLocator));
}
async function resetToDefault() {
    await driver.switchTo().defaultContent();
}
async function checkFileExists(path) {
    // try {
        // console.log(fs.existsSync(path))
        if (fs.existsSync(path)) {
          await expect(true).toBeTruthy();
        } else {
            await expect(false).toBeTruthy();
        }
    //   } catch(err) {
    //     // console.error(err)
    //     await expect(false).toBeTruthy();
    //   }
}
async function deleteFileBeforeDownload(path) {
if (fs.existsSync(path)) {
fs.unlink(path, async function (err) {
    if (err) {
        await expect(false).toBeTruthy();
        throw err;
    }
    await expect(true).toBeTruthy();
});
}
}

function getSortDirection(arr, expectedOrder,isDate,isNumber) {
    const pattern = /[\/\-_]/g;
    let c = [];
    const newArr = arr.filter(value => value !== " ")
    .map(value => value.toLowerCase())
    // .map(value => value.replace(pattern, ''))
    if (isDate) {
      for (let i = 1; i < newArr.length - 1; i++) {
        if (moment(newArr[i], "DD/MM/YYYY HH:mm:ss") > moment(newArr[i - 1], "DD/MM/YYYY HH:mm:ss")) {
          c.push(-1);
        } else if (moment(newArr[i], "DD/MM/YYYY HH:mm:ss") < moment(newArr[i - 1], "DD/MM/YYYY HH:mm:ss")) {
          c.push(1);
        } else {
          c.push(0);
        }
      }
    } else if (isNumber) {
      for (let i = 1; i < newArr.length; i++) {
              if (Number(newArr[i]) < Number(newArr[i - 1])) {
                c.push(1);
              } else if (Number(newArr[i]) > Number(newArr[i - 1])) {
                c.push(-1);
              } else {
                c.push(0);
              }
        }
    } else {
        for (let i = 1; i < newArr.length; i++) {
            if (newArr[i] < newArr[i - 1]) {
                c.push(1);
            } else if (newArr[i] > newArr[i - 1]) {
                c.push(-1);
            } else {
                c.push(0);
            }
          }
    }
    if (c.every((n) => n == 0 )) return expectedOrder;
    if (c.every((n) => n <= 0)) return 'ascending';
    if (c.every((n) => n >= 0)) return 'descending';
    return 'unsorted';
}

function writeArrayToJsonFile(dataArray, filePath) {
    // Convert the array of objects to a JSON string
    const jsonData = JSON.stringify(dataArray, null, 2); // The third argument (2) adds indentation for readability
  
    // Check if the file exists
    if (fs.existsSync(filePath)) {
      // If the file exists, delete it before creating the new file
      try {
        fs.unlinkSync(filePath);
        console.log('Existing file deleted successfully!');
      } catch (err) {
        console.error('Error deleting existing file:', err);
      }
    }
  
    // Write the JSON data to the new file
    fs.writeFile(filePath, jsonData, (err) => {
      if (err) {
        console.error('Error writing to file:', err);
      } else {
        console.log('Data has been written to the file successfully!');
      }
    });
  }

  function compareArrayOfRights(array1, array2) {
    const result = [];
  
    // Loop through each item in array1 (version 4 data)
    array1.forEach((item1) => {
      // Find the corresponding item in array2 (version 5 data) based on the operationNum
      const matchedItem = array2.find((item2) => item2.operationNum === item1.operationNum);
  
      if (matchedItem) {
        // If the current operationNum exists in both versions (array1 and array2)
        const diff = {
            comparaison : true,
            operationNum: item1.operationNum,
            operationName: item1.operationName,
            exist_in_v4: true,
            exist_in_v5: true,
            bloc: [],
        };
  
        // Loop through each bloc in the current operationNum
        item1.bloc.forEach((bloc1) => {
          // Find the corresponding bloc in array2 based on the blocName
          const matchedBloc = matchedItem.bloc.find((bloc2) => bloc2.blocName === bloc1.blocName);
          const diffBloc = {
            blocName: bloc1.blocName,
            exist_in_v4: true,
            exist_in_v5: true,
            action_in_v4: [],
            action_in_v5: [],
            action_granted_in_v4: [], 
            action_granted_in_v5: [], 
            action_granted_inherited_in_v4: [],
            action_granted_inherited_in_v5: [],
            switch_in_v4: bloc1.switch,
            switch_in_v5: matchedBloc ? matchedBloc.switch : false,
          };
  
          if (matchedBloc) {
            // If the current bloc exists in both versions (array1 and array2)
            // Check for the actions that exist in version 4 but not in version 5
            bloc1.action.forEach((action1) => {
              if (!matchedBloc.action.includes(action1)) {
                diff.comparaison = false
                diffBloc.action_in_v4.push(action1);
              }
            });
  
            // Check for the actions that exist in version 5 but not in version 4
            matchedBloc.action.forEach((action2) => {
              if (!bloc1.action.includes(action2)) {
                diff.comparaison = false
                diffBloc.action_in_v5.push(action2);
              }
            });
            const newArray = bloc1.action_granted.filter(action => !bloc1.action_granted_inherited.includes(action));
            newArray.forEach((action1) => {
              if (!matchedBloc.action_granted.includes(action1)) {
                diff.comparaison = false
                diffBloc.action_granted_in_v4.push(action1);
              }
            });
  
            // Check for the actions that exist in version 5 but not in version 4
            matchedBloc.action_granted.forEach((action2) => {
              if (!newArray.includes(action2)) {
                diff.comparaison = false
                diffBloc.action_granted_in_v5.push(action2);
              }
            });

            bloc1.action_granted_inherited.forEach((action1) => {
              if (!matchedBloc.action_granted_inherited.includes(action1)) {
                diff.comparaison = false
                diffBloc.action_granted_inherited_in_v4.push(action1);
              }
            });
  
            // Check for the actions that exist in version 5 but not in version 4
            matchedBloc.action_granted_inherited.forEach((action2) => {
              if (!bloc1.action_granted_inherited.includes(action2)) {
                diff.comparaison = false
                diffBloc.action_granted_inherited_in_v5.push(action2);
              }
            });
          } else {
            // If the current bloc is not present in version 5, add it to the diffBloc
            diff.comparaison = false
            diffBloc.exist_in_v5 = false
            diffBloc.action_in_v4 = bloc1.action.slice();
          }
  
          // Add the diffBloc to the current operationNum differences
          diff.bloc.push(diffBloc);
        });
  
        // Add the diff object to the overall differences array
        result.push(diff);
      } else {
        // If the current operationNum is not present in version 5, add it to the result with appropriate data
        result.push({
            comparaison : false,
          operationNum: item1.operationNum,
          operationName: item1.operationName,
          exist_in_v4: true,
          exist_in_v5: false,
            bloc: item1.bloc.map((bloc) => ({
            blocName: bloc.blocName,
            exist_in_v4: true,
            exist_in_v5: false,
            action_in_v4: bloc.action.slice(),
            action_in_v5: [],
            action_granted_in_v4: bloc.action_granted.slice(), 
            action_granted_in_v5: [], 
            action_granted_inherited_in_v4: bloc.action_granted_inherited.slice(),
            action_granted_inherited_in_v5: [],
            switch_in_v4: bloc.switch,
            switch_in_v5: false,
          })),
        });
      }
    });
  
    // Check for operations present in version 5 but not in version 4
    array2.forEach((item2) => {
        const matchedItem = array1.find((item1) => item1.operationNum === item2.operationNum);
      
        if (!matchedItem) {
          // If the current operationNum is not present in version 4, add it to the result with appropriate data
          result.push({
            comparaison : false,
            operationNum: item2.operationNum,
            operationName: item2.operationName,
            exist_in_v4: false,
            exist_in_v5: true,
            bloc: item2.bloc.map((bloc) => ({
                blocName: bloc.blocName,
                exist_in_v4: false,
                exist_in_v5: true,
                action_in_v4: [],
                action_in_v5: bloc.action.slice(),
                action_granted_in_v4: [], 
                action_granted_in_v5: bloc.action_granted.slice(), 
                action_granted_inherited_in_v4: [],
                action_granted_inherited_in_v5: bloc.action_granted_inherited.slice(),
                switch_in_v4: false,
                switch_in_v5: bloc.switch,
            })),
           
          });
        } else {
          // If the operationNum exists in version 4, check for missing bloc in item2 compared to item1
          const missingBlocInItem2 = item2.bloc.filter((bloc2) => !matchedItem.bloc.some((bloc1) => bloc2.blocName === bloc1.blocName));
          
          if (missingBlocInItem2.length > 0) {
            // Find the object in the result array with the same operationNum
            const existingResultItem = result.find((item) => item.operationNum === item2.operationNum);
      
            // Add the missing bloc from item1 to item2 in the existing result object
            existingResultItem.bloc.push(...missingBlocInItem2.map((bloc) => ({
              blocName: bloc.blocName,
                exist_in_v4: false,
                exist_in_v5: true,
                action_in_v4: [],
                action_in_v5: bloc.action.slice(),
                action_granted_in_v4: [], 
                action_granted_in_v5: bloc.action_granted.slice(), 
                action_granted_inherited_in_v4: [],
                action_granted_inherited_in_v5: bloc.action_granted_inherited.slice(),
                switch_in_v4: false,
                switch_in_v5: bloc.switch,
            })));
          }
        }
      });
      return result.filter(el => el.comparaison == false);
  }

function compareArrayOfRightsEntity(array1, array2) {
    const result = [];

    array1.forEach((item1) => {
        const matchedItem = array2.find((item2) => item2.entityName === item1.entityName);

        if (!matchedItem) {
            result.push({
                comparaison : false,
                entityName : item1.entityName,
                exist_in_v4: true,
                exist_in_v5: false,
                entities_in_v4 : item1.entities,
                entities_in_v5 : []
            })
        }else {
            const diff = {
                comparaison : true,
                entityName: item1.entityName,
                exist_in_v4: true,
                exist_in_v5: true,         
                entities_in_v4: [],
                entities_in_v5: [],
            };

            item1.entities.forEach(entity1 => {
                const matchedEntity = matchedItem.entities.find(entity2 => entity2 == entity1)

                if (!matchedEntity) {
                    diff.entities_in_v4.push(entity1)
                }
            })

            result.push(diff)
        }
    
    })

    array2.forEach((item2) => {
        const matchedItem = array1.find((item1) => item1.entityName === item2.entityName);
        
        if (!matchedItem) {
            result.push({
                comparaison : false,
                entityName : item2.entityName,
                exist_in_v4: false,
                exist_in_v5: true,
                entities_in_v4 : [],
                entities_in_v5 : item2.entities
            })
        } else {
            const missingEntityInItem2 = item2.entities.filter((entity2) => !matchedItem.entities.some((entity1) => entity2 === entity1));
            
            if (missingEntityInItem2.length > 0) {
            const existingResultItem = result.find((item) => item.entityName === item2.entityName);
        
            existingResultItem.entities_in_v5.push(...missingEntityInItem2)
            }
        }
        });
        return result;
}

function compareArrayOfMenus(arrayFr, arrayEn) {
const result = [];

arrayFr.forEach((item1) => {
    const matchedItem = arrayEn.find((item2) => item2.menuId === item1.menuId);

    if (matchedItem) {

        if (matchedItem.menus[0].vues == false && item1.menus[0].vues == true) {
                diff = {
                menuId: item1.menuId,
                menuName_fr: item1.menuName,
                menuName_en: matchedItem.menuName,
                exist_in_fr: true,
                exist_in_en: true,
                menus: item1.menus.map(menu => ({
                    vues: true,
                    vueId: menu.vueId,
                    vueFr: menu.vueFr,
                    vueEn: "** Not Exist **",
                    })),
            };
            result.push(diff);
        }else if (matchedItem.menus[0].vues == true && item1.menus[0].vues == false) {
                diff = {
                menuId: matchedItem.menuId,
                menuName_fr: item1.menuName,
                menuName_en: matchedItem.menuName,
                exist_in_fr: true,
                exist_in_en: true,
                menus: matchedItem.menus.map(menu => menu.vueFr == "** Not Exist **"),
            };
            result.push(diff);
        }else if (matchedItem.menus[0].vues == true && item1.menus[0].vues == true) {
            diff = {
                menuId: item1.menuId,
                menuName_fr: item1.menuName,
                menuName_en: matchedItem.menuName,
                exist_in_fr: true,
                exist_in_en: true,
                menus: [],
            };
            item1.menus.forEach((vue1) => {
                const matchedMenu = matchedItem.menus.find((vue2) => vue2.vueId === vue1.vueId);

                    if (matchedMenu) {
                        diffMenu = {
                            vues : true,
                            vueId: vue1.vueId,
                            vueFr: vue1.vueFr,
                            vueEn: matchedMenu.vueEn,
                        };
                    } else {
                        diffMenu = {
                            vues : true,
                            vueId: vue1.vueId,
                            vueFr: vue1.vueFr,
                            vueEn: "** Not Exist **",
                        };
                    }
                    diff.menus.push(diffMenu);
                });
                result.push(diff);
        }else {
            diff = {
                menuId: matchedItem.menuId,
                menuName_fr: item1.menuName,
                menuName_en: matchedItem.menuName,
                exist_in_fr: true,
                exist_in_en: true,
                menus: [{vues : false}],
            };
            result.push(diff);
        }
    } else {
        result.push({
            menuId: item1.menuId,
            menuName: item1.menuName,
            menuName_en: "** Not Exist **",
            exist_in_fr: true,
            exist_in_en: false,
            menus: item1.menus.map((menu) => (menu.vues == false ? {vues : false} : {
                vues : true,
                vueId: menu.vueId,
                vueFr: menu.vueFr,
                vueEn: "** Not Exist **",
            })),
        });
    }
});

arrayEn.forEach((item2) => {
    const matchedItem = arrayFr.find((item1) => item1.menuId === item2.menuId);

    if (!matchedItem) {
        result.push({
        menuId: item2.menuId,
        menuName_fr: "** Not Exist **",
        menuName_en: item2.menuName,
        exist_in_fr: false,
        exist_in_en: true,
        menus: item2.menus.map((menu) => ({
            vues: true,
            vueId: menu.vueId,
            vueFr: "** Not Exist **",
            vueEn: menu.vueEn,
        })),
        });
    } else {

        if (matchedItem.menus[0].vues == true && item2.menus[0].vues == true) {
            const missingMenusInItem2 = item2.menus.filter(
                (vue2) => !matchedItem.menus.some((vue1) => vue2.vueId === vue1.vueId)
                );
                
                if (missingMenusInItem2.length > 0) {
                const existingResultItem = result.find((item) => item.menuId === item2.menuId);
                existingResultItem.menus.push(
                    ...missingMenusInItem2.map((menu) => ({
                    vues: true,
                    vueId: menu.vueId,
                    vueFr: "** Not Exist **",
                    vueEn: menu.vueEn,
                    }))
                );
                }
        }
    }
    });
// console.log(JSON.stringify(result, null, 2));
return result;
}

function compareDataVues (array1, array2){
    const result = [];

    for (const text1 of array1) {
        existedText = array2.find(text2 => text2 == text1)
        if (!existedText){
            const obj1 = {
                different: true,
                text: text1,
                exist_in_v42: false,
                exist_in_v5: true
            };
    
            result.push(obj1);
        }
    }

    for (const text2 of array2) {
        existedText = array1.find(text1 => text1 == text2)

        if (!existedText) {
            const obj2 = {
                different: true,
                text: text2,
                exist_in_v42: true,
                exist_in_v5: false
            };
    
            result.push(obj2);
        }
    }

    return result;
}
  

async function countFilesDownloaded(path, action) {
    if (fs.existsSync(path)) {
        fs.readdir(path, async (err, files) => {
            if(action == 'save')
            countFiles = files.length;
            else
            await expect(countFiles + 1).toEqual(files.length);
          });
    }
}

async function loadIt(document, i) {
    let tc;
    if (i == -1) tc = document;
    else tc = document.step[i];
    it(tc.name, async () => {

        switch (tc.type) {
            case OPENURL:
                let val = tc.url;
                await driver.get(val);
                await expectSpec(tc);
                break;
            case FILLRIGHTS:
                    if(tc.iframe) await turnToIframe(tc.iframe);
                    for (let index in tc.rights) {
                        let objet = tc.rights[index];
                        if (!Array.isArray(tc.rights))
                            objet = tc.input;
                        let inputLocator, inputElement;

                        switch (objet.type) {
                            case EXPANDDIV:
                            inputLocator = await getLocator(objet);
                            inputElement = await driver.findElement(inputLocator);
                            await inputElement.click();
                            if(tc.iframe) await resetToDefault();
                            await waitUntilLoaderDisappear(tc.loading);
                            if(tc.iframe) await turnToIframe(tc.iframe);
                            await driver.sleep(100);
                            break;
                            case BUTTON:
                                inputLocator = await getLocator(objet);
                                inputElement = await driver.findElement(inputLocator);
                                await inputElement.click();
                                await driver.sleep(100);
                                break;
                            case JS:
                                await driver.executeScript(objet._);
                                if(tc.iframe) await resetToDefault();
                                await waitUntilLoaderDisappear(tc.loading);
                                if(tc.iframe) await turnToIframe(tc.iframe);
                                break;
                            case SWITCH:
                                inputLocator = await getLocator(objet);
                                inputElement = await driver.findElements(inputLocator);
                                for (let index in inputElement) {
                                await inputElement[index].click();
                                }
                                break;
                        }          
                    }
                    if(tc.iframe) await resetToDefault();
                    // if(tc.loading)
                    // await waitUntilLoaderDisappear(tc.loading);
                    await expectSpec(tc);
                break;
            case ALERT :
                    try {
                        await driver.wait(until.alertIsPresent(), run_time.elementLocatedTimeOut);
                        await driver.switchTo().alert().accept();
                        break;
                    } catch (error) {}
            case FILLINPUT:
                if(tc.iframe) await turnToIframe(tc.iframe);
                for (let input in tc.input) {
                    let objet = tc.input[input];
                    if (!Array.isArray(tc.input))
                        objet = tc.input;
                    let inputLocator, inputElement;
                    switch (objet.type) {
                        case "switchToWindow":
                            const windows = await driver.getAllWindowHandles();
                            await driver.switchTo().window(windows[objet.window]);
                            break
                        case "getText" :
                            inputLocator = await getLocator(objet);
                            inputElement = await driver.findElement(inputLocator);
                            savedText = await inputElement.getAttribute("value")
                            break;
                        case TEXT:
                            inputLocator = await getLocator(objet);
                            inputElement = await driver.findElement(inputLocator);
                            inputElement.clear();
                            if (objet.savedValue) {
                                await inputElement.sendKeys(savedText);
                                break;
                            }
                            if(objet._ == "saved")
                            await inputElement.sendKeys(numLastRow);
                            else if(objet._ && objet._ != "saved") {
                                let val = objet._;
                                await inputElement.sendKeys(val);
                                if(objet.date)
                                await inputElement.sendKeys(Key.ENTER);
                            }
                            if(objet.space)
                            await inputElement.sendKeys(Key.SPACE);
                            if(objet.enter)
                            await inputElement.sendKeys(Key.ENTER);
                            break;
                            case SELECT:
                            if (objet.double) {
                                inputLocator = await getLocator(objet);
                                await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                                inputElement = await driver.findElement(inputLocator);
                                options = await inputElement.findElements(By.xpath("//select[@id='" + objet._ + "']//option"));
                                let seenOptions = new Set();
                                let hasNotDoubleOption = true;
                                let countDouble = 0;
                                let arrayOfDoubleVal = []
                                let arrayOfDoubleText = []
                                for (const option of options) {
                                let optionText = await option.getText();
                                let optionVal = await option.getAttribute("value");
                                let optionValUpc = optionVal.toUpperCase()
                                if (optionValUpc.includes(".")) {
                                    indexOfPoint = optionValUpc.indexOf(".");
                                    lastIndex = optionValUpc.length;
                                    optionValUpc = optionValUpc.substring(indexOfPoint + 1, lastIndex);
                                  }
                                  
                                  if (seenOptions.has(optionValUpc)) {
                                    hasNotDoubleOption = false;
                                    countDouble++;
                                    arrayOfDoubleVal.push(optionVal.substring(1));
                                    arrayOfDoubleText.push(optionText);
                                    // break;
                                  }
                                seenOptions.add(optionValUpc);
                                }
                                if (countDouble !== 0) {
                                    console.log(` => ** ${countDouble} Values on double : ${arrayOfDoubleVal} and their text is : ${arrayOfDoubleText}  **`)
                                }
                                expect(hasNotDoubleOption).toBeTrue();
                                break;
                            }
                            inputLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                            inputElement = await driver.findElement(inputLocator);
                            await clickWhenClickable(driver, inputLocator, run_time.elementToBeClickable);
                            let val = objet._;
                            await selectItemOnSelectByXpath(driver, objet.value, val);
                            break;
                        case BUTTON:
                            if (printBloc){
                                await driver.sleep(500);
                                inputLocator = await getLocator(objet);
                                inputElement = await driver.findElement(inputLocator);
                                await inputElement.click();
                                await driver.sleep(500);
                                break;
                            }else{
                                break;
                            } 
                        case DOUBLE:
                            inputLocator = await getLocator(objet);
                            inputElement = await driver.findElement(inputLocator);
                            await driver.actions().doubleClick(inputElement).perform();
                            break;
                        case FILE:
                            inputLocator = await getLocator(objet);
                            inputElement = await driver.findElement(inputLocator);
                            await inputElement.sendKeys(objet._);
                            break;
                        case RADIO_POPUP:
                            inputLocator = By.xpath("//td[normalize-space() = '" + objet._ + "']");
                            inputElement = await driver.findElement(inputLocator);
                            await driver.actions().doubleClick(inputElement).perform();
                            break;
                        case CHECK:
                            try {
                                inputLocator = await getLocator(objet);
                                const checkElement = await driver.findElement(inputLocator);
                                await checkElement.click();
                                break;
                            } catch (error) {
                                console.log("*** No line exist to check ***")
                                break;
                            }
                        case SCROLL:
                            await driver.executeScript(`window.scrollBy(0,${objet._})`);
                            break;
                        case WAIT:
                            await driver.sleep(objet._)
                            break;
                        case JS:
                            if(objet._.includes("export")) 
                            await deleteFileBeforeDownload(objet.file);
                            await driver.executeScript(objet._);
                            break;
                        case ALERT :
                            try {
                            await driver.wait(until.alertIsPresent(), run_time.elementLocatedTimeOut);
                            await driver.switchTo().alert().accept();
                            break;
                            } catch (error) {}                           
                        case LOADING :
                            await waitUntilLoaderDisappear(objet);
                            break;
                        case EXECUTE :
                                countFilesDownloaded(objet._,objet.action)
                                break;
                        break;
                        case LENGTH :
                            try {
                                inputLocator = await getLocator(objet);
                                await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                                inputElement = await driver.findElement(inputLocator);
                                lineTable = await inputElement.findElements(By.xpath(objet.value));
                                countTr = lineTable.length;
                                break;
                            } catch (error) {
                                countTr = 0;
                                break;
                            }
                            case "not-here":
                                printBloc = false;
                                try{
                                    inputLocator = await getLocator(objet);
                                    inputElement = await driver.findElements(inputLocator);
                                    if (inputElement){
                                        printBloc = true;
                                    }else{
                                        break;
                                    }
                                    }catch(e){}

                    }

                }
                if(tc.name == "logout")
                {
                    await driver.wait(until.alertIsPresent(), run_time.elementLocatedTimeOut);
                    await driver.switchTo().alert().accept();
                }
                if(tc.iframe) await resetToDefault();
                if(tc.loading)
                await waitUntilLoaderDisappear(tc.loading);
                await expectSpec(tc);
                break;
            case VALUE:
                if(tc.iframe) await turnToIframe(tc.iframe);
                inputLocator = await getLocator(tc.input);
                inputElement = await driver.findElement(inputLocator);
                let value = inputElement.getAttribute("value");
                await value.then(async (res) => {
                    if(tc.input.saveType == "string")
                    nameSign = res;
                    else
                    numLastRow = res;
                    if(tc.iframe) await resetToDefault();
                    await expectSpec(tc);
                });
            break;
            case COMPARETABLE:
                if(tc.iframe) await turnToIframe(tc.iframe);
                let isDate = false;
                let isNumber = false;
                let inputsTexts = [];

                for (let index in tc.input) {
                    let objet = tc.input[index];
                    if (objet.isDate){
                        isDate = true;
                    }
                    if (objet.isNumber){
                        isNumber = true;
                    }
                    switch (objet.type) {
                        case BUTTON:
                            await driver.sleep(3000);
                            inputButtonLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputButtonLocator), run_time.elementLocatedTimeOut);
                            inputButtonElement = await driver.findElement(inputButtonLocator);
                            await inputButtonElement.click();
                            await driver.sleep(2000);
                            break;
                        case ALERT :
                            await driver.wait(until.alertIsPresent(), run_time.elementLocatedTimeOut);
                            await driver.switchTo().alert().accept();
                            break;
                        case WAIT :
                            await driver.sleep(objet._)
                            break;
                        case LOADING :
                            await waitUntilLoaderDisappear(objet);
                            break;
                        default: 
                            inputLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                            inputElement = await driver.findElement(inputLocator);
                            inputText = await inputElement.getText();
                            inputsTexts.push(inputText);
                            break;
                    }
                }
                expectedOrder = tc.check.value;
                actualOrder = getSortDirection(inputsTexts,expectedOrder,isDate,isNumber);
                await expectSpecParam(actualOrder,expectedOrder);
                if(tc.iframe) await resetToDefault();
                break;
            case INNERTEXT:
            if(tc.iframe) await turnToIframe(tc.iframe);
            inputLocator = await getLocator(tc.check);
            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
            inputElement = await driver.findElement(inputLocator);
            await inputElement.getText().then(async (res) => {
                // const firstChar = res.charAt(0);
                // if(isLetter(firstChar)) {
                //     res = res.substring(1);
                // }
                numLastRow = res;
                if(tc.iframe) await resetToDefault();
                await expectSpec(tc);
            });
            break;
            case BUTTON:
                if(tc.iframe) await turnToIframe(tc.iframe);
                inputLocator = await getLocator(tc.button);
                inputElement = await driver.findElement(inputLocator);
                await inputElement.click();
                if(tc.iframe) await resetToDefault();
                expectSpec(tc);
                break;
            case SUBMIT:
                const submitLocator = await getLocator(tc.submit);
                const submitElement = await driver.findElement(submitLocator);
                await submitElement.click();
                await driver.wait(until.urlIs(tc.submit._), run_time.elementLocatedTimeOut);
                break;
            case JS:
                if(tc.iframe) await turnToIframe(tc.iframe);
                if(tc.new) {
                    const windows = await driver.getAllWindowHandles();
                    await driver.switchTo().window(windows[2]);
                    await driver.executeScript(tc.input);
                    await driver.switchTo().window(windows[1]);
                }
                else
                await driver.executeScript(tc.input);
                if(tc.iframe) await resetToDefault();
                if(tc.loading)
                await waitUntilLoaderDisappear(tc.loading);
                await expectSpec(tc);
                break;
            case SEARCH_MENU_ITEM : 
                for (let input in tc.input) {
                 let objet = tc.input[input];
                 if (!Array.isArray(tc.input))
                     objet = tc.input;
                 let inputLocator, inputElement;
                 switch (objet.type) {
                     case TEXT:
                         inputLocator = await getLocator(objet);
                         inputElement = await driver.findElement(inputLocator);
                         inputElement.clear();
                         await inputElement.sendKeys(objet._);
                         await driver.sleep(2000);
                         break;
                     case BUTTON:
                         inputLocator = await getLocator(objet);
                         inputElement = await driver.findElement(inputLocator);
                         await inputElement.click();
                         await driver.sleep(1000);
                         break;
                 }
 
             }
             if(tc.loading)
             await waitUntilLoaderDisappear(tc.loading);
             await expectSpec(tc);
             break;
 
            case HOVERMENU :
                for (let item in tc.item) {
                    let objet = tc.item[item];
                    if (!Array.isArray(tc.item))
                        objet = tc.item;
                    let inputLocator, inputElement;
                    if (objet.type === HOVER) {
                        inputLocator = await getLocator(objet);
                        await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                        inputElement = await driver.findElement(inputLocator);
                        await driver.actions().move({origin: inputElement}).perform();
                    }
                    else if (objet.type === CLICK) {
                        inputLocator = await getLocator(objet);
                        await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                        inputElement = await driver.findElement(inputLocator);
                        await driver.actions().click(inputElement).perform();
                        await driver.actions().move({x: 80, y: 80}).perform();
                    }
                    else if (objet.type === DOUBLECLICK) {
                        inputLocator = await getLocator(objet);
                        await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                        inputElement = await driver.findElement(inputLocator);
                        await driver.actions().doubleClick(inputElement).perform();
                        await driver.actions().move({x: 80, y: 80}).perform();
                    }
                    else if (objet.type === WAIT) {
                        await driver.sleep(objet._)
                    }
                    
                }
                await driver.sleep(1000);
                await expectSpec(tc);
                break;
            case LOADING :
                if(tc.iframe) await turnToIframe(tc.iframe);
                await waitUntilLoaderDisappear(tc.loading);
                if(tc.iframe) await resetToDefault();
                await expectSpec(tc);
            break;
            case NAVMENU:
                if (tc.menu) {
                    const menuLocator = await getLocator(tc.menu);
                    await driver.findElement(menuLocator).click();
                    await tc.menu.item.forEach(async (element, index) => {
                        if (element.type === JS) {
                            await driver.executeScript(element._);
                            if (tc.menu.item.length - 1 == index) {
                                await driver.sleep(2000);
                                await expectSpec(tc);
                            }
                        }
                        else if (element.type === XPATH) {
                            xpathLocator = By.xpath("//*[contains(text(),'" + element._ + "')]");
                            await driver.wait(until.elementLocated(xpathLocator), run_time.elementLocatedTimeOut);
                            const eventElement = await driver.findElements(xpathLocator);
                            await eventElement[1].click();
                        }

                    });
                }
                break;
            case LOGOUT:
                inputLocator = await getLocator(tc.button);
                inputElement = await driver.findElement(inputLocator);
                await inputElement.click();
                await driver.wait(until.alertIsPresent(), run_time.elementLocatedTimeOut);
                await driver.switchTo().alert().accept();
                await driver.sleep(3000);
                expectSpec(tc);
                break;
            case ONLYCHECK:
                    await expectSpec(tc);
            break;
            case WAIT:
                await driver.sleep(tc.wait)
                expectSpec(tc);
                break;
            case COMPAREVERSION:
                if(tc.iframe) await turnToIframe(tc.iframe);
                for (let input in tc.input) {
                    let objet = tc.input[input];
                    if (!Array.isArray(tc.input))
                        objet = tc.input;
                    let inputLocator, inputElement;
                    switch (objet.type) {
                        case "comparevuesv42" :
                            inputLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                            inputElement = await driver.findElement(inputLocator);                            
                            
                            existedButton = true;
                            while (existedButton) {
                                await driver.sleep(8000);
                                inputElement = await driver.wait(until.elementLocated(By.xpath("//div[@class=' dhxtoolbar_float_right']//div[@class='dhxtoolbar_text']")), run_time.elementLocatedTimeOut);
                                await driver.sleep(8000);
                                arrayOfPages = await inputElement.findElements(By.xpath("//div[@class=' dhxtoolbar_float_right']//div[@class='dhxtoolbar_text']"));

                                if (arrayOfPages.length !== 0){

                                    let arrayOfNumberPages = []
                                    for (arrayOfPage of arrayOfPages) {
                                        pageText = await arrayOfPage.getText()
                                            arrayOfNumberPages.push(pageText)
                                    }
    
                                    for (arrayOfNumberPage of arrayOfNumberPages) {
                                        if (arrayOfNumberPage == "1") {
                                            await driver.sleep(8000);
                                            inputElement = await driver.wait(until.elementLocated(By.xpath("//table[@class='obj row20px']//tr//td[2]//a")), run_time.elementLocatedTimeOut);
                                            await driver.sleep(8000);
                                            arrayOfLigneTables = await inputElement.findElements(By.xpath("//table[@class='obj row20px']//tr//td[2]//a"));
                                        
                                            for (arrayOfLigneTable of arrayOfLigneTables){
                                                arrayOfLigneTableText = await driver.wait(until.elementIsEnabled(arrayOfLigneTable), run_time.elementLocatedTimeOut);
                                                caseText = await arrayOfLigneTableText.getText()
                                            arrayOfDataVuesVersionFourTwo.push(caseText)
                                        }
                                        }else {
                                            await driver.sleep(8000);
                                            let openPage = await driver.findElement(By.xpath("//div[@class=' dhxtoolbar_float_right']//div[@class='dhxtoolbar_text' and text()='"+arrayOfNumberPage+"']//parent::node()"));
                                            openPage.click();
                                            await driver.sleep(4000);
                                            inputElement = await driver.wait(until.elementLocated(By.xpath("//table[@class='obj row20px']//tr//td[2]//a")), run_time.elementLocatedTimeOut);
                                            await driver.sleep(4000);
                                            arrayOfLigneTables = await inputElement.findElements(By.xpath("//table[@class='obj row20px']//tr//td[2]//a"));
                                        
                                            for (arrayOfLigneTable of arrayOfLigneTables){
                                                arrayOfLigneTableText = await driver.wait(until.elementIsEnabled(arrayOfLigneTable), run_time.elementLocatedTimeOut);
                                                caseText = await arrayOfLigneTableText.getText()
                                                arrayOfDataVuesVersionFourTwo.push(caseText)
                                            }
                                        }    
                                    }
                                }else {
                                    arrayOfLigneTables = await inputElement.findElements(By.xpath("//table[@class='obj row20px']//tr//td[2]//a"));
    
                                for (arrayOfLigneTable of arrayOfLigneTables){
                                    arrayOfLigneTableText = await driver.wait(until.elementIsEnabled(arrayOfLigneTable), run_time.elementLocatedTimeOut);
                                    caseText = await arrayOfLigneTableText.getText()
                                    arrayOfDataVuesVersionFourTwo.push(caseText)
                                }
                                }

                                buttonRights = await driver.findElements(By.xpath("//img[@src='./assets/images/imgs/toolbar/ar_right.gif']//parent::node()"));
                                
                                if (buttonRights.length !== 0) {
                                    buttonRights[0].click()
                                    await driver.sleep(8000);
                                }else {
                                    existedButton = false;
                                } 
                            }
                        return;
                        case "comparevuesv43" :
                            inputLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                            inputElement = await driver.findElement(inputLocator);

                            existedButton = true;
                            while (existedButton) {
                                await driver.sleep(4000);
                                inputElement = await driver.wait(until.elementLocated(By.xpath("//div[@class=' dhxtoolbar_float_right']//div[@class='dhxtoolbar_text']")), run_time.elementLocatedTimeOut);
                                await driver.sleep(4000);
                                arrayOfPages = await inputElement.findElements(By.xpath("//div[@class=' dhxtoolbar_float_right']//div[@class='dhxtoolbar_text']"));

                                if (arrayOfPages.length !== 0){

                                    let arrayOfNumberPages = []
                                    for (arrayOfPage of arrayOfPages) {
                                        pageText = await arrayOfPage.getText()
                                            arrayOfNumberPages.push(pageText)
                                    }
    
                                    for (arrayOfNumberPage of arrayOfNumberPages) {
                                        await driver.sleep(4000);
                                        if (arrayOfNumberPage == "1") {
                                            inputElement = await driver.wait(until.elementLocated(By.xpath("//table[@class='obj row20px']//tr//td[2]//a")), run_time.elementLocatedTimeOut);
                                            await driver.sleep(4000);
                                            arrayOfLigneTables = await inputElement.findElements(By.xpath("//table[@class='obj row20px']//tr//td[2]//a"));
                                        
                                            for (arrayOfLigneTable of arrayOfLigneTables){
                                                arrayOfLigneTableText = await driver.wait(until.elementIsEnabled(arrayOfLigneTable), run_time.elementLocatedTimeOut);
                                                caseText = await arrayOfLigneTableText.getText()
                                            arrayOfDataVuesVersionFourThree.push(caseText)
                                        }
                                        }else {
                                            let openPage = await driver.findElement(By.xpath("//div[@class=' dhxtoolbar_float_right']//div[@class='dhxtoolbar_text' and text()='"+arrayOfNumberPage+"']//parent::node()"));
                                            openPage.click();
                                            await driver.sleep(4000);
                                            inputElement = await driver.wait(until.elementLocated(By.xpath("//table[@class='obj row20px']//tr//td[2]//a")), run_time.elementLocatedTimeOut);
                                            await driver.sleep(4000);
                                            arrayOfLigneTables = await inputElement.findElements(By.xpath("//table[@class='obj row20px']//tr//td[2]//a"));
                                        
                                            for (arrayOfLigneTable of arrayOfLigneTables){
                                                arrayOfLigneTableText = await driver.wait(until.elementIsEnabled(arrayOfLigneTable), run_time.elementLocatedTimeOut);
                                                caseText = await arrayOfLigneTableText.getText()
                                                arrayOfDataVuesVersionFourThree.push(caseText)
                                            }
                                        }    
                                    }
                                }else {
                                    arrayOfLigneTables = await inputElement.findElements(By.xpath("//table[@class='obj row20px']//tr//td[2]//a"));
    
                                for (arrayOfLigneTable of arrayOfLigneTables){
                                    arrayOfLigneTableText = await driver.wait(until.elementIsEnabled(arrayOfLigneTable), run_time.elementLocatedTimeOut);
                                                caseText = await arrayOfLigneTableText.getText()
                                    arrayOfDataVuesVersionFourThree.push(caseText)
                                }
                                }
                                buttonRights = await driver.findElements(By.xpath("//img[@src='./assets/images/imgs/toolbar/ar_right.gif']//parent::node()"));
                                
                                if (buttonRights.length !== 0) {
                                    buttonRights[0].click()
                                    await driver.sleep(4000);
                                }else {
                                    existedButton = false;
                                } 
                            }

                            const result = compareDataVues(arrayOfDataVuesVersionFourThree,arrayOfDataVuesVersionFourTwo)
                            writeArrayToJsonFile(result,filePathDataVues)
                        return;
                        case "comparevuesv5" :
                            inputLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                            inputElement = await driver.findElement(inputLocator);
                            
                                inputElement = await driver.wait(until.elementLocated(By.xpath("//ul[@class='pagination pagination-sm mb-0']")), run_time.elementLocatedTimeOut);
                                await driver.sleep(8000);
                                arrayOfPages = await inputElement.findElements(By.xpath("//ul[@class='pagination pagination-sm mb-0']"));

                                if (arrayOfPages.length !== 0){

                                    buttonFin = await driver.findElements(By.xpath("//a[@aria-label='Fin']//parent::node()"));
                                    buttonFin[0].click()
                                    await driver.sleep(8000);

                                    inputElement = await driver.wait(until.elementLocated(By.xpath("//li[@class='page-item active disabled']//a")), run_time.elementLocatedTimeOut);
                                    lastPage = await inputElement.findElement(By.xpath("//li[@class='page-item active disabled']//a")).getText();

                                    for (let index = 1; index <= lastPage; index++) {
                                        inputElement = await driver.findElement(By.xpath("//li[@class='page-item']//a[text()='"+index+"']"));
                                        pageButton = await inputElement.findElement(By.xpath("//li[@class='page-item']//a[text()='"+index+"']")).click();
                                        await driver.sleep(8000);

                                        inputElement = await driver.wait(until.elementLocated(By.xpath("//table[@class='obj row20px']//tr//td[2]//a")), run_time.elementLocatedTimeOut);
                                        await driver.sleep(8000);
                                        arrayOfLigneTables = await inputElement.findElements(By.xpath("//table[@class='obj row20px']//tr//td[2]//a"));
                                        
                                        for (arrayOfLigneTable of arrayOfLigneTables){
                                            arrayOfLigneTableText = await driver.wait(until.elementIsEnabled(arrayOfLigneTable), run_time.elementLocatedTimeOut);
                                            caseText = await arrayOfLigneTableText.getText()
                                            arrayOfDataVuesVersionFive.push(caseText)
                                        }
                                    }
                                } else {
                                    arrayOfLigneTables = await inputElement.findElements(By.xpath("//table[@class='obj row20px']//tr//td[2]//a"));
    
                                    for (arrayOfLigneTable of arrayOfLigneTables){
                                        arrayOfLigneTableText = await driver.wait(until.elementIsEnabled(arrayOfLigneTable), run_time.elementLocatedTimeOut);
                                        caseText = await arrayOfLigneTableText.getText()
                                        arrayOfDataVuesVersionFive.push(caseText)
                                    }
                                }

                            const resultCompareDataVue = compareDataVues(arrayOfDataVuesVersionFourTwo,arrayOfDataVuesVersionFive)
                            writeArrayToJsonFile(resultCompareDataVue,filePathDataVues)
                        return;
                        case "compareRightsV42":
                            inputLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                            inputElement = await driver.findElement(inputLocator);
 
                            // Get all operation options and create an array to store their numbers
                            arrayOperationOptions = await inputElement.findElements(By.xpath("//select[@id='CBNOMTABLE']//option"));
                            arrayOperationNums = [];

                            // Loop through all options and create an object for each operation
                            for (let arrayOperationOption of arrayOperationOptions) {
                                let operationObject = null;
                                operationNum = await arrayOperationOption.getAttribute("value");
                                operationName = await arrayOperationOption.getAttribute("title");

                                if (operationNum !== null && operationNum !== '') {
                                arrayOperationNums.push(operationNum);
                                operationObject = {
                                    operationNum: operationNum,
                                    operationName:operationName,
                                    bloc: []
                                };
                                }

                                if (operationObject !== null) {
                                arrayofOpeartionObjectVersionFourTwo.push(operationObject);
                                }
                            }

                            // Loop through each operation and perform actions
                            for (let arrayOptionNum of arrayOperationNums) {
                                // Select operation and click button "add all"
                                
                                arrayOptionsElement = await inputElement.findElement(By.xpath("//select[@id='CBNOMTABLE']//option[@value='" + arrayOptionNum + "']")).click();
                                await driver.sleep(1000);
                                inputLocator = By.xpath("//input[@id='addAll']");
                                inputElement = await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                                addAllButton = await inputElement.findElement(inputLocator).click();

                                // Get all blocs of the selected operation
                                operationBlocNums = await inputElement.findElements(By.xpath("//table[@id='blocAction']//tr"));
                                arrayOperationBlocNums = [];

                                for (let operationBlocNum of operationBlocNums) {
                                if (operationBlocNum !== null) {
                                    blocNum = await operationBlocNum.getAttribute("id");
                                    if (blocNum !== null && blocNum !== '') {
                                    arrayOperationBlocNums.push(blocNum);
                                    }
                                }
                                }

                                for (let opeartionBlocNum of arrayOperationBlocNums) {
                                let arrayOfAction = []
                                let arrayOfActionGranted = []
                                let arrayOfActionGrantedInherited = []
                                operationBlocText = await inputElement.findElement(By.xpath("//tr[@id='" + opeartionBlocNum + "']//td[2]")).getText();
                                
                                operationBlocActions = await inputElement.findElements(By.xpath("//tr[@id='" + opeartionBlocNum + "']//td[3]//ul//li//p"));
                                for (let operationBlocAction of operationBlocActions) {
                                    operationBlocActionText = await operationBlocAction.getText();
                                    arrayOfAction.push(operationBlocActionText);
                                }
                                opeartionSousBlocActionsGranted = await inputElement.findElements(By.xpath("//tr[@id='" + opeartionBlocNum + "']//td[3]//ul//li[@class='token-input-selected-token token-input-token']//p"));
                                for (let opeartionSousBlocActionGranted of opeartionSousBlocActionsGranted){
                                    let opeSousBlocActionGrantedText = await opeartionSousBlocActionGranted.getText();
                                    arrayOfActionGranted.push(opeSousBlocActionGrantedText)
                                }
                                

                                let opeartionBlocNumSub = opeartionBlocNum.substring(0, opeartionBlocNum.indexOf(':'));
                                const excludedValues = ["lot","lots","ordres","ordre",,"factures","facture","paiement","paiements"];
                                for (let opeartionObjectVersionFourTwo of arrayofOpeartionObjectVersionFourTwo) {
                                    if (opeartionBlocNumSub === opeartionObjectVersionFourTwo.operationNum) {
                                    const blocNameExists = opeartionObjectVersionFourTwo.bloc.some(item => item.blocName === operationBlocText);
                                    if (!blocNameExists) {
                                        let objBloc = {
                                        blocName: operationBlocText,                                        
                                        action : arrayOfAction,
                                        action_granted : arrayOfActionGranted,
                                        action_granted_inherited : arrayOfActionGrantedInherited,
                                        switch : false
                                        };
                                        blocNameExistsSwitch = objBloc.action.find(item => item == "Central")
                                        if (blocNameExistsSwitch && excludedValues.includes(operationBlocText.toLowerCase())) {
                                            objBloc.switch = true;
                                            newAction = objBloc.action.filter(item => item !== "Central")
                                            opeartionObjectVersionFourTwo.bloc.push({...objBloc, action : newAction});
                                        }else {
                                            opeartionObjectVersionFourTwo.bloc.push(objBloc);
                                        }
                                    } else {
                                        // Find the existing bloc and append the actions to it
                                        const existingBloc = opeartionObjectVersionFourTwo.bloc.find(item => item.blocName === operationBlocText);

                                        if (existingBloc) {
                                            opeartionSousBlocActionsGrantedInherited = await inputElement.findElements(By.xpath("//tr[@id='" + opeartionBlocNum + "']//td[3]//ul//li[contains(@class,'token-input-token-readonly token-input-selected-token token-input-token')]//p"));
                                            for (let opeartionSousBlocActionGrantedInherited of opeartionSousBlocActionsGrantedInherited){
                                                let opeSousBlocActionGrantedInheritedText = await opeartionSousBlocActionGrantedInherited.getText();
                                                arrayOfActionGrantedInherited.push(opeSousBlocActionGrantedInheritedText)
                                            }
                                            existingBloc.action_granted_inherited.push(...arrayOfActionGrantedInherited)

                                        }
                                    }
                                    break;
                                    }
                                }
                                }
                                
                            }
                            return;
                        case "compareRightsV43":
                            inputLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                            inputElement = await driver.findElement(inputLocator);
 
                            arrayOperationOptions = await inputElement.findElements(By.xpath("//select[@id='CBNOMTABLE']//option"));
                            arrayOperationNums = [];

                            for (let arrayOperationOption of arrayOperationOptions) {
                                let operationObject = null;
                                operationNum = await arrayOperationOption.getAttribute("value");
                                operationName = await arrayOperationOption.getAttribute("title");

                                if (operationNum !== null && operationNum !== '') {
                                arrayOperationNums.push(operationNum);
                                operationObject = {
                                    operationNum: operationNum,
                                    operationName:operationName,
                                    bloc: []
                                };
                                }

                                if (operationObject !== null) {
                                    arrayofOpeartionObjectVersionFourThree.push(operationObject);
                                }
                            }

                            for (let arrayOptionNum of arrayOperationNums) {
                                arrayOptionsElement = await inputElement.findElement(By.xpath("//select[@id='CBNOMTABLE']//option[@value='" + arrayOptionNum + "']")).click();
                                await driver.sleep(1000);
                                inputLocator = By.xpath("//input[@id='addAll']");
                                inputElement = await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                                addAllButton = await inputElement.findElement(inputLocator).click();

                                operationBlocNums = await inputElement.findElements(By.xpath("//table[@id='blocAction']//tr"));
                                arrayOperationBlocNums = [];

                                for (let operationBlocNum of operationBlocNums) {
                                if (operationBlocNum !== null) {
                                    blocNum = await operationBlocNum.getAttribute("id");
                                    if (blocNum !== null && blocNum !== '') {
                                    arrayOperationBlocNums.push(blocNum);
                                    }
                                }
                                }

                                for (let opeartionBlocNum of arrayOperationBlocNums) {
                                arrayOperationBlocActions = [];
                                operationBlocText = await inputElement.findElement(By.xpath("//tr[@id='" + opeartionBlocNum + "']//td[2]")).getText();
                                operationBlocActions = await inputElement.findElements(By.xpath("//tr[@id='" + opeartionBlocNum + "']//td[3]//ul//li//p"));

                                for (let operationBlocAction of operationBlocActions) {
                                    operationBlocActionText = await operationBlocAction.getText();
                                    arrayOperationBlocActions.push(operationBlocActionText);
                                }

                                let opeartionBlocNumSub = opeartionBlocNum.substring(0, opeartionBlocNum.indexOf(':'));
                                const excludedValues = ["lot","lots","ordres","ordre",,"factures","facture","paiement","paiements"];
                                for (let opeartionObjectVersionFourThree of arrayofOpeartionObjectVersionFourThree) {
                                    if (opeartionBlocNumSub === opeartionObjectVersionFourThree.operationNum) {
                                    const blocNameExists = opeartionObjectVersionFourThree.bloc.some(item => item.blocName === operationBlocText);
                                    if (!blocNameExists) {
                                        let objBloc = {
                                        blocName: operationBlocText,
                                        action: arrayOperationBlocActions,
                                        switch : false
                                        };
                                        blocNameExistsSwitch = objBloc.action.find(item => item == "Central")
                                        if (blocNameExistsSwitch && excludedValues.includes(operationBlocText.toLowerCase())) {
                                            objBloc.switch = true;
                                            newAction = objBloc.action.filter(item => item !== "Central")
                                            opeartionObjectVersionFourThree.bloc.push({...objBloc, action : newAction});
                                        }else {
                                            opeartionObjectVersionFourThree.bloc.push(objBloc);
                                        }
                                    } else {
                                        const existingBloc = opeartionObjectVersionFourThree.bloc.find(item => item.blocName === operationBlocText);
                                        blocNameExistsSwitch = arrayOperationBlocActions.find(item => item == "Central")
                                        if (blocNameExistsSwitch && excludedValues.includes(operationBlocText.toLowerCase())) {
                                            existingBloc.switch = true;
                                            newAction = arrayOperationBlocActions.filter(item => item !== "Central")
                                            existingBloc.action = [...existingBloc.action, ...newAction];
                                        }else{
                                            existingBloc.action = [...existingBloc.action, ...arrayOperationBlocActions];
                                        }
                                    }
                                    break;
                                    }
                                }
                                }
                            }
                            result = compareArrayOfRights(arrayofOpeartionObjectVersionFour,arrayofOpeartionObjectVersionFourThree)
                            writeArrayToJsonFile(result, filePathRights);
                            return; 
                        case "compareRightsV5":
                            inputLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                            inputElement = await driver.findElement(inputLocator);
                            operationUls = await inputElement.findElements(By.xpath("//ul[@class='operations-list']//li"));

                            for (let operationUl of operationUls) {
                                let operationNumber = await operationUl.getAttribute("data-operation-id");
                                if (operationNumber !== null) {
                                let operationName = await inputElement.findElement(By.xpath("//li[@data-operation-id='" + operationNumber + "']//div[@class='operation-header']//label")).getText();
                                
                                    operationObject = {
                                        operationNum: operationNumber,
                                        operationName: operationName,
                                        bloc: []
                                    };
                                    arrayofOpeartionObjectVersionFive.push(operationObject);
    
                                    let expanddivElement = await driver.findElement(By.xpath("//li[@data-operation-id='" + operationNumber + "']//label"));
                                    expanddivElement.click();
                                    let operationBlocUls = await inputElement.findElements(By.xpath("//li[@data-operation-id='" + operationNumber + "']//ul[@class='operation-blocks']//li"));
    
                                    for (let operationBlocUl of operationBlocUls) {
                                        let opeartionBloc = await operationBlocUl.getAttribute("data-block-id");
                                        let opeartionBlocName = await inputElement.findElement(By.xpath("//li[@data-operation-id='" + operationNumber + "']//ul[@class='operation-blocks']//li[@data-block-id='" + opeartionBloc + "']//div[@class='block-label']//label")).getText();
                                        let arrayOfAction = []
                                        let arrayOfActionGranted = []
                                        let arrayOfActionGrantedInherited = []

                                        let opeartionSousBlocAction = await inputElement.findElements(By.xpath("//li[@data-operation-id='" + operationNumber + "']//ul[@class='operation-blocks']//li[@data-block-id='" + opeartionBloc + "']//div[@class='block-row']//div[@class='category-actions']//div[contains(@class,'action')]"));
                                        for (let opeSousBlocAction of opeartionSousBlocAction){
                                            let opeSousBlocActionText = await opeSousBlocAction.getText();
                                            arrayOfAction.push(opeSousBlocActionText)
                                        }
                                        
                                        let opeartionSousBlocActionsGranted = await inputElement.findElements(By.xpath("//li[@data-operation-id='" + operationNumber + "']//ul[@class='operation-blocks']//li[@data-block-id='" + opeartionBloc + "']//div[@class='block-row']//div[@class='category-actions']//div[@class='action granted']"));
                                        for (let opeartionSousBlocActionGranted of opeartionSousBlocActionsGranted){
                                            let opeSousBlocActionGrantedText = await opeartionSousBlocActionGranted.getText();
                                            arrayOfActionGranted.push(opeSousBlocActionGrantedText)
                                        }
                                        
                                        let opeartionSousBlocActionsGrantedInherited = await inputElement.findElements(By.xpath("//li[@data-operation-id='" + operationNumber + "']//ul[@class='operation-blocks']//li[@data-block-id='" + opeartionBloc + "']//div[@class='block-row']//div[@class='category-actions']//div[contains(@class,'action granted inherited')]"));
                                        for (let opeartionSousBlocActionGrantedInherited of opeartionSousBlocActionsGrantedInherited){
                                            let opeSousBlocActionGrantedInheritedText = await opeartionSousBlocActionGrantedInherited.getText();
                                            arrayOfActionGrantedInherited.push(opeSousBlocActionGrantedInheritedText)
                                        }
                                        
                                        let objBloc = {
                                            blocName: opeartionBlocName,
                                            action : arrayOfAction,
                                            action_granted : arrayOfActionGranted,
                                            action_granted_inherited : arrayOfActionGrantedInherited,
                                            switch : false
                                        };
    
                                        try {
                                            let opeartionBlocCentral = await inputElement.findElement(By.xpath("//li[@data-operation-id='" + operationNumber + "']//ul[@class='operation-blocks']//li[@data-block-id='" + opeartionBloc + "']//div[@class='category-actions central']//div[@class='action central-local']")).getAttribute("data-action-id");
                                            if (opeartionBlocCentral){
                                                objBloc.switch = true
                                            }  
                                        } catch (error) {} 
                                        operationObject.bloc.push(objBloc);
                                    
                                    }
                                }
                            }
                            result = compareArrayOfRights(arrayofOpeartionObjectVersionFour,arrayofOpeartionObjectVersionFive)
                            writeArrayToJsonFile(result, filePathRights);
                            return;
                        case "compareRightsEntityV5":
                            inputLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                            inputElement = await driver.findElement(inputLocator);
                            entityTypes = await inputElement.findElements(By.xpath("//span[@class='standartTreeRow']"));

                            for (let entityType of entityTypes.slice(2)) {
                                entityTypeElement = await driver.wait(until.elementIsEnabled(entityType), run_time.elementLocatedTimeOut);
                                entityTypeText = await entityType.getText();
                                arrayOfEntityCodes = []
                                
                                entityTypeElement = await driver.findElement(By.xpath(`//span[contains(text(), "${entityTypeText}")]`));
                                await entityTypeElement.click();
                                await driver.sleep(8000);
                                let entityTypeCodes = await inputElement.findElements(By.xpath("//table[@class='obj row20px']//tr//td[1]"));

                                for (let entityTypeCode of entityTypeCodes) {
                                    entityTypeCodeElement = await driver.wait(until.elementIsEnabled(entityTypeCode), run_time.elementLocatedTimeOut);
                                    let entityTypeCodeText = await entityTypeCodeElement.getText();
                                    arrayOfEntityCodes.push(entityTypeCodeText)
                                }

                                entityObjet = {
                                    entityName: entityTypeText,
                                    entities : arrayOfEntityCodes,
                                };
                                arrayofEntityRightsVersionFive.push(entityObjet);
                            }

                            resultOfCompareRightsEntity = compareArrayOfRightsEntity(arrayofEntityRightsVersionFour,arrayofEntityRightsVersionFive)
                            writeArrayToJsonFile(resultOfCompareRightsEntity, filePathEntity);
                            return;
                        case "compareRightsEntityV4":
                        inputLocator = await getLocator(objet);
                        await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                        inputElement = await driver.findElement(inputLocator);
                        entityTypes = await inputElement.findElements(By.xpath("//span[@class='standartTreeRow']"));

                        for (let entityType of entityTypes.slice(2)) {
                            entityTypeElement = await driver.wait(until.elementIsEnabled(entityType), run_time.elementLocatedTimeOut);
                            entityTypeText = await entityType.getText();
                            arrayOfEntityCodes = []
                            
                            entityTypeElement = await driver.findElement(By.xpath(`//span[contains(text(), "${entityTypeText}")]`));
                            await entityTypeElement.click();
                            await driver.sleep(8000);
                            let entityTypeCodes = await inputElement.findElements(By.xpath("//table[@class='obj row20px']//tr//td[1]"));

                            for (let entityTypeCode of entityTypeCodes) {
                                entityTypeCodeElement = await driver.wait(until.elementIsEnabled(entityTypeCode), run_time.elementLocatedTimeOut);
                                let entityTypeCodeText = await entityTypeCodeElement.getText();
                                arrayOfEntityCodes.push(entityTypeCodeText)
                            }

                            entityObjet = {
                                entityName: entityTypeText,
                                entities : arrayOfEntityCodes,
                            };
                            arrayofEntityRightsVersionFour.push(entityObjet);
                        }         
                        return;
                        case "getDataFr" :
                        inputLocator = await getLocator(objet);
                        await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                        inputElement = await driver.findElement(inputLocator);
                        arrayOfNavs = await inputElement.findElements(By.xpath(`//ul[@class='nav flex-column']//li[@class='nav-item dropright']//i`));

                        arrayOfNavsElems = []
                        for (arrayOfNav of arrayOfNavs){
                            savedNav= await arrayOfNav.getAttribute(`class`)
                            if(savedNav !== null){
                                arrayOfNavsElems.push(savedNav)
                            }
                        }

                        for (arrayOfNavsElem of arrayOfNavsElems) {
                            inputElement = await driver.findElement(By.xpath("//ul/li/a/i[@class='" + arrayOfNavsElem + "']//parent::node()//parent::node()"))
                            await driver.actions().move({origin: inputElement}).perform();
                            await driver.sleep(1000);
                            moduleOfNavbarDashboards = await inputElement.findElements(By.xpath("//ul/li/a/i[@class='" + arrayOfNavsElem +  "']//parent::node()//parent::node()//ul//li//a"));
                            arrayOfModules = []
                            for (moduleOfNavbarDashboard of moduleOfNavbarDashboards) {
                                moduleOfNavbarDashboardAttribute = await moduleOfNavbarDashboard.getAttribute(`data-fonction-id`)
                                if (moduleOfNavbarDashboardAttribute !== null) {
                                    arrayOfModules.push(moduleOfNavbarDashboardAttribute)
                                }
                            }

                            for (arrayOfModule of arrayOfModules) {
                                inputElement = await driver.findElement(By.xpath("//ul/li/a/i[@class='" + arrayOfNavsElem + "']//parent::node()//parent::node()"))
                                await driver.actions().move({origin: inputElement}).perform();
                                await driver.sleep(1000);
                                let expanddivElement = await driver.findElement(By.xpath("//ul/li/a/i[@class='" + arrayOfNavsElem +  "']//parent::node()//parent::node()//ul//li//a[@data-fonction-id='"+arrayOfModule+"']//parent::node()"));
                                expanddivElement.click();
                                await driver.sleep(1000);
                                await driver.actions().move({x: 80, y: 80}).perform();
                                menusElements = await inputElement.findElements(By.xpath("//ul[@class='nav nav-pills']//li//button"));

                                arrayOfmenus = []

                                if (menusElements.length !== 0) {
                                    for (menusElement of menusElements) {
                                        menuText = await menusElement.getText()
                                        menuID = await menusElement.getAttribute("data-fonction-id")
                                        arrayOfmenus.push({
                                                menuId : menuID,
                                                menuName : menuText,
                                                menus : []
                                            })
                                            
                                    }
                                    arrayofMenusObjectVersionFr.push(...arrayOfmenus)
                                    arrayOfVuesID = []
                                    arrayOfVuesText = []
                                    for (arrayOfmenu of arrayOfmenus){
                                        if (arrayOfmenu.menuName.includes("'") && !arrayOfmenu.menuName.includes("Piste d'Audit")) {
                                            newMenuName = arrayOfmenu.menuName.replace(/'/g, '&apos;');
                                            vuesOfMenus = await inputElement.findElements(By.xpath('//ul[@class="nav nav-pills"]//li//button[text()="'+newMenuName+'"]//parent::node()//div//a'));
                                            }else {
                                                vuesOfMenus = await inputElement.findElements(By.xpath('//ul[@class="nav nav-pills"]//li//button[text()="'+arrayOfmenu.menuName+'"]//parent::node()//div//a'));
                                            }
                                        
                                        if (vuesOfMenus.length == 0) {
                                            for (arrayOfObj of arrayofMenusObjectVersionFr) {
                                                if (arrayOfObj.menuId == arrayOfmenu.menuId){
                                                    arrayOfObj.menus.push({
                                                        vues : false
                                                    })
                                                }
                                            }
                                        }else{
                                            inputElement = await driver.findElement(By.xpath('//ul[@class="nav nav-pills"]//li//button[text()="'+arrayOfmenu.menuName+'"]//parent::node()'))
                                            await driver.actions().move({origin: inputElement}).perform();
                                            for (vueOfMenu of vuesOfMenus) {
                                                vueText = await vueOfMenu.getText()
                                                vueId = await vueOfMenu.getAttribute("data-fonction-id")
                                                for (arrayOfObj of arrayofMenusObjectVersionFr) {
                                                    if (arrayOfObj.menuId == arrayOfmenu.menuId){
                                                        arrayOfObj.menus.push({
                                                            vues : true,
                                                            vueId : vueId,
                                                            vueFr : vueText,
                                                            vueEn : '',
                                                        })
                                                    }
                                                }
                                        }
                                        await driver.actions().move({x: 80, y: 80}).perform();
                                        }
                                    
                                    }
                                    inputElement = await driver.findElement(By.xpath("//li[@class='nav-item active']//a[@class='btn-tabbar-close']"))
                                    inputElement.click();
                                }
                            }
                        }
                        return;
                        case "getDataEn" :
                            inputLocator = await getLocator(objet);
                            await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                            inputElement = await driver.findElement(inputLocator);
                            arrayOfNavs = await inputElement.findElements(By.xpath(`//ul[@class='nav flex-column']//li[@class='nav-item dropright']//i`));

                            arrayOfNavsElems = []
                            for (arrayOfNav of arrayOfNavs){
                            savedNav= await arrayOfNav.getAttribute(`class`)
                                if(savedNav !== null){
                                    arrayOfNavsElems.push(savedNav)
                                }
                            }

                            for (arrayOfNavsElem of arrayOfNavsElems) {
                                inputElement = await driver.findElement(By.xpath("//ul/li/a/i[@class='" + arrayOfNavsElem + "']//parent::node()//parent::node()"))
                                await driver.actions().move({origin: inputElement}).perform();
                                await driver.sleep(1000);
                                moduleOfNavbarDashboards = await inputElement.findElements(By.xpath("//ul/li/a/i[@class='" + arrayOfNavsElem +  "']//parent::node()//parent::node()//ul//li//a"));
                                arrayOfModules = []
                                for (moduleOfNavbarDashboard of moduleOfNavbarDashboards) {
                                    moduleOfNavbarDashboardAttribute = await moduleOfNavbarDashboard.getAttribute(`data-fonction-id`)
                                    if (moduleOfNavbarDashboardAttribute !== null) {
                                        arrayOfModules.push(moduleOfNavbarDashboardAttribute)
                                    }
                                }

                                for (arrayOfModule of arrayOfModules) {
                                    inputElement = await driver.findElement(By.xpath("//ul/li/a/i[@class='" + arrayOfNavsElem + "']//parent::node()//parent::node()"))
                                    await driver.actions().move({origin: inputElement}).perform();
                                    await driver.sleep(1000);
                                    let expanddivElement = await driver.findElement(By.xpath("//ul/li/a/i[@class='" + arrayOfNavsElem +  "']//parent::node()//parent::node()//ul//li//a[@data-fonction-id='"+arrayOfModule+"']//parent::node()"));
                                    expanddivElement.click();
                                    await driver.sleep(1000);
                                    await driver.actions().move({x: 80, y: 80}).perform();
                                    menusElements = await inputElement.findElements(By.xpath("//ul[@class='nav nav-pills']//li//button"));

                                    arrayOfmenus = []

                                    if (menusElements.length !== 0) {
                                        for (menusElement of menusElements) {
                                            menuText = await menusElement.getText()
                                            menuId = await menusElement.getAttribute("data-fonction-id")
                                            arrayOfmenus.push({
                                                    menuId : menuId,
                                                    menuName : menuText,
                                                    menus : []
                                                })
                                                
                                        }
                                        arrayofMenusObjectVersionEn.push(...arrayOfmenus)
                                        for (arrayOfmenu of arrayOfmenus){
    
                                            if (arrayOfmenu.menuName.includes("'")  && !arrayOfmenu.menuName.includes("Piste d'Audit")) {
                                                newMenuName = arrayOfmenu.menuName.replace(/'/g, '&apos;');
                                                vuesOfMenus = await inputElement.findElements(By.xpath('//ul[@class="nav nav-pills"]//li//button[text()="'+newMenuName+'"]//parent::node()//div//a'));
                                                }else {
                                                    vuesOfMenus = await inputElement.findElements(By.xpath('//ul[@class="nav nav-pills"]//li//button[text()="'+arrayOfmenu.menuName+'"]//parent::node()//div//a'));
                                                }
    
                                            if (vuesOfMenus.length == 0) {
                                                for (arrayOfObj of arrayofMenusObjectVersionEn) {
                                                    if (arrayOfObj.menuId == arrayOfmenu.menuId){
                                                        arrayOfObj.menus.push({
                                                            vues : false
                                                        })
                                                    }
                                                }
                                            }else{
                                                inputElement = await driver.findElement(By.xpath('//ul[@class="nav nav-pills"]//li//button[text()="'+arrayOfmenu.menuName+'"]//parent::node()'))
                                                await driver.actions().move({origin: inputElement}).perform();
                                                for (vueOfMenu of vuesOfMenus) {
                                                    vueText = await vueOfMenu.getText()
                                                    vueId = await vueOfMenu.getAttribute("data-fonction-id")
                                                    for (arrayOfObj of arrayofMenusObjectVersionEn) {
                                                        if (arrayOfObj.menuId == arrayOfmenu.menuId){
                                                            arrayOfObj.menus.push({
                                                                vues : true,
                                                                vueId : vueId,
                                                                vueFr : '',
                                                                vueEn : vueText,
                                                            })
                                                        }
                                                    }
                                                }
                                                await driver.actions().move({x: 80, y: 80}).perform();
                                            }                                   
                                        }
                                        inputElement = await driver.findElement(By.xpath("//li[@class='nav-item active']//a[@class='btn-tabbar-close']"))
                                        inputElement.click();
                                    }
                                }
                            }
                            result = compareArrayOfMenus(arrayofMenusObjectVersionFr,arrayofMenusObjectVersionEn)
                            writeArrayToJsonFile(result, filePathLangueVues);
                                return;
                            case "dataTableV4" :
                                inputLocator = await getLocator(objet);
                                await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                                inputElement = await driver.findElement(inputLocator);

                                const options_v4 = await driver.findElements(By.xpath("//select[@id='CBNOMTABLE']//option"));
                                const textArray_v4 = [];

                                for (const option of options_v4) {
                                const text = await option.getText();
                                textArray_v4.push(text);
                                }
                            return;
                            case "dataTableV5" :
                                inputLocator = await getLocator(objet);
                                await driver.wait(until.elementLocated(inputLocator), run_time.elementLocatedTimeOut);
                                inputElement = await driver.findElement(inputLocator);

                                const options_v5 = await driver.findElements(By.xpath("//select[@id='CBNOMTABLE']//option"));
                                const textArray_v5 = [];

                                for (const option of options_v5) {
                                const text = await option.getText();
                                textArray_v5.push(text);
                                }
                                const res = compareVues(textArray_v4,textArray_v5)
                            return;
                        }
                        

                }
                if(tc.name == "logout")
                {
                    await driver.wait(until.alertIsPresent(), run_time.elementLocatedTimeOut);
                    await driver.switchTo().alert().accept();
                }
                if(tc.iframe) await resetToDefault();
                if(tc.loading)
                await waitUntilLoaderDisappear(tc.loading);
                await expectSpec(tc);
                break;
            
            }


    });

}

loadjs().then(async (res) => {
    parser.parseString(res, async function (error, result) {
        document = result;
        await describe('DLF : ',  function()  {
            beforeAll(async () => {
                driver = await buildDriver();
            });
            if (Array.isArray(document.step)) {
                for (let i in document.step) {
                   loadIt(document, i).then(async () => {
                    })
                }
            } else {
                loadIt(document.step, -1).then(async () => {
                })
            }

            afterAll(async () => {
              //  await closeDriver(driver);
            });
    });
  });
});