const OPENURL = 'openurl';
const FILLINPUT = 'fillinput';
const TEXT = 'text';
const FIELD = 'field';
const SUBMIT = 'submit';
const HOVER = 'hover';
const CLICK = 'click';
const CHECK = 'check';
const TAG = 'tag';
const URL = 'url';
const NAVMENU = 'navMenu';
const HOVERMENU = 'hoverMenu';
const JS = 'js';
const BUTTON = 'button';
const LINK = 'link';
const SELECT = 'select';
const SCROLL = 'scroll'
const SCENARIO = 'scenario';
const FILE = 'file';
const WAIT = 'wait';
const LOGOUT = 'logout'
const TABLE = 'table';
const COLUMN = 'column';
const SEARCH_MENU_ITEM = 'searchMenuItem';
const IFRAME= 'iframe';
const ROWS= 'rows';
const SEARCHCHECK= 'searchCheck';
const TABLECOUNT= 'table-count';
const DOUBLECLICK= 'doubleClick';
const INNERTEXT= 'innerText';
const VALUE= 'value';
const SWITCH= 'switch';
const ONLYCHECK= 'only-check';
const FILLRIGHTS= 'fillRights';
const COMPARE= 'compare';
const CHECKINDEX = 'checkIndex';
const NOTHERE = 'not-here';
const CHECKNAME = 'checkName';
const CHECKFILE = 'check-file';
const EXPANDDIV = 'expandDiv';
const DOUBLE = 'double';
const ALERT = 'alert';
const LOADING = 'loading';
const EXECUTE = 'execute';
const COMPARETABLE = 'compare-table';
const COMPAREVALUE = 'compare-value';
const CHECKVALUE = 'check-value';
const LENGTH = 'length';
const COMPAREVERSION = 'compare-version';

module.exports = {
    OPENURL, FILLINPUT, TEXT, SUBMIT, CHECK, TAG, URL, NAVMENU, JS, BUTTON, SELECT, SCROLL, LINK, SCENARIO,
    FILE, WAIT, LOGOUT, TABLE, COLUMN,DOUBLECLICK,ROWS,INNERTEXT,VALUE,FILLRIGHTS,SWITCH,COMPARE,
    SEARCH_MENU_ITEM,FIELD,IFRAME,SEARCHCHECK,TABLECOUNT,HOVERMENU,HOVER,CLICK,ONLYCHECK,CHECKINDEX,NOTHERE,CHECKNAME,
    CHECKFILE,EXPANDDIV,DOUBLE,ALERT,LOADING,EXECUTE,COMPARETABLE,COMPAREVALUE,CHECKVALUE,LENGTH,COMPAREVERSION
};
