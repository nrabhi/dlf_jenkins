/**
 * @author Nadhem Med Rached
 * @email nrached@teklend.com
 */
/*jshint esversion: 8 */

const {
    Store
} = require("fs-json-store");
const store = new Store({
    file: "data.json"
});
const {
    By,
    until,
    Builder
} = require('selenium-webdriver');
const {
    run_time,
    headless,
    maximized,
    navigator,
    screenSize,
    browsers
} = require('./config');

//const README_FILE_PATH = "./src/main/webapp/src/tests/e2e/readme.md";
const chrome = require('selenium-webdriver/chrome');
const firefox = require('selenium-webdriver/firefox');
const ie = require('selenium-webdriver/ie');
const edge = require('selenium-webdriver/edge');
let currentDriver;
/**
 * Build driver and set default setting
 * TODO: rename this function
 */
async function buildDriver() {

    let tabs = await currentDriver.getAllWindowHandles();
    if (tabs.length > 1) {
        for (let index = 1; index < tabs.length; index++) {
            await currentDriver.switchTo().window(tabs[index]);
            await currentDriver.close();
        }
        await currentDriver.switchTo().window(tabs[0]);

    }
    await currentDriver.executeScript("window.open('');");
    tabs = await currentDriver.getAllWindowHandles();

    await currentDriver.switchTo().window(tabs[1]);
    if (maximized) {
        await currentDriver.manage().window().maximize();
    }

    return currentDriver;

}

/**
 * This function is used in the jasmine config file to initiate the driver before executing the test 
 */
async function prepareDriver() {
    let chromeOptions = new chrome.Options().windowSize(screenSize);
    let firefoxOption = new firefox.Options().windowSize(screenSize);
    let edgeOption = new edge.Options().windowSize(screenSize);
    let ieOption = new ie.Options().addArguments('--start-maximized');
    
    switch (navigator) {
        case "CHROME":
            if (headless) {
                chromeOptions.headless();
                chromeOptions.addArguments('--disable-gpu');
            }
            //onglet privé
            //chromeOptions.addArguments("--incognito")
            chromeOptions.addArguments(`user-data-dir=${__dirname}\\user-data`);
            break;
        case "FIREFOX":
            if (headless) {
                firefoxOption = firefoxOption.headless();
            }
            firefoxOption.addArguments(`user-data-dir=${__dirname}\\user-data`);
            break;
        case "EDGE":
            if (headless) {
                edgeOption.headless();
            }
            edgeOption.addArguments(`user-data-dir=${__dirname}\\user-data`);
            break;
    }

    const driver = await new Builder()
        .forBrowser(browsers[navigator] == 'ie' ? 'internet explorer' : browsers[navigator])
        .setChromeOptions(chromeOptions)
        .setFirefoxOptions(firefoxOption)
        .setEdgeOptions(edgeOption)
        .setIeOptions(ieOption)
        .build();

    if (navigator.toLowerCase() !== browsers.IE) {
        if (maximized) {
            await driver.manage().window().maximize();
        }
    }

    await driver.manage().setTimeouts({ implicit: run_time.implicitTimeOut, pageLoad: run_time.implicitTimeOut });
    currentDriver = driver;
    
}


function makeRandomName(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
function makeRandomEmail() {
    return "username" + Math.floor(Math.random() * 10000) + "@teklen.com";
}

/**
 * Making a random integer
 * @param {lenght of the random int to be generated} length 
 */
function makeRandomInt(length) {
    var result = '';
    var characters = '123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

/**
 * Generate random fload number
 * @param {*} min 
 * @param {*} max 
 */
function makeRandomFloat(min, max) {
    var random = Math.random() * (+max - +min) + min;
    return Math.round(random);
}

/**
 * Loop throught array of element and wait untill the iteration of all items
 * @param {*} array 
 * @param {*} callback 
 */
async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}
async function scrollToTop(driver, x, y) {
    await driver.executeScript('scroll(' + x + ', ' + y + ');'); // TODO move to helper scrollToTop()

}
/**
 * ClickWhenCickable or saveClick, this function will prevent the error of Element is not clickable
 * @param {*} driver 
 * @param {*} locator 
 * @param {*} timeout 
 */
function clickWhenClickable(driver, locator, timeout) {
    return driver.wait(function () {
        return driver.findElement(locator).then(function (element) {
            return element.click().then(function () {
                return true;
            }, function () {
                return false;
            });
        }, function () {
            return false;
        });
    }, timeout, 'Timeout waiting for ' + locator);
}

/**
 * This function simulate humain typing to trigger form change
 * @param {*} element 
 * @param {*} keys 
 * @param {*} driver 
 */
async function asyncSendKeys(element, keys, driver) {
    if (navigator.toLowerCase() !== browsers.IE) {
        await element.sendKeys(keys);
    } else {
        for (let index = 0; index < keys.length; index++) {
            await element.sendKeys(keys[index]);
            await driver.sleep(60);
        }
    }

}

/**
 * This function selects a text 
 * @param {*} node  
 */
function SelectNodeText(node) {
    node = document.querySelector(node);
    console.log(node);
    if (document.body.createTextRange) {
        const range = document.body.createTextRange();
        range.moveToElementText(node);
        range.select();
    } else if (window.getSelection) {
        const selection = window.getSelection();
        const range = document.createRange();
        range.selectNodeContents(node);
        selection.removeAllRanges();
        selection.addRange(range);
    } else {
        console.log("Could not select text in node: Unsupported browser.");
    }
}


/**
 * In case we need soft sleep this function will handle it by conditionaly 
 * @param {*} driver 
 */
async function softSleep(driver) {
    await driver.sleep(100);
}

/**
 * This function will clean and close browsers and it will hold anything specfics for each browser
 * @param {*} driver 
 */
async function closeDriver(driver) {
    if (navigator.toLowerCase() !== browsers.IE) {
        await currentDriver.close();
        await currentDriver.quit();
        // const tabs = await currentDriver.getAllWindowHandles();
        // await currentDriver.switchTo().window(tabs[0]);
    }
}

/**
 * Close the intiated driver (navigator)
 */
function closeCurrentDriver() {
    return currentDriver.quit();
}
function makeRandomDate() {
    start = new Date();
    end = new Date();
    var d = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime())),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear() - 10;

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return ([day, month, year].join('/')).toString();
}
function waitForVisibleElement(driver, locator, timeout) {
    var element = driver.wait(until.elementLocated(locator), timeout);
    return driver.wait(new until.elementLocated('for element to be visible ' + locator, function () {
        return element.isDisplayed().then(v => v ? element : null);
    }), timeout);
};
async function selectItemOnSelectByXpath(driver, id, value) {
    const optiontElement = await driver.findElement(By.xpath("//select[@id='" + id + "']/option[@value='" + value + "']"));
    await driver.wait(until.elementIsEnabled(optiontElement), run_time.elementLocatedTimeOut);
    const actions = driver.actions({ bridge: true });
    await actions.move({ duration: 4000, origin: optiontElement, x: 0, y: 0 });
    await optiontElement.click();
}
module.exports = {
    waitForVisibleElement,
    makeRandomName,
    makeRandomInt,
    makeRandomFloat,
    asyncForEach,
    clickWhenClickable,
    buildDriver,
    asyncSendKeys,
    softSleep,
    closeDriver,
    prepareDriver,
    closeCurrentDriver,
    SelectNodeText,
    makeRandomEmail,
    scrollToTop,
    selectItemOnSelectByXpath
};