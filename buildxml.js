const fs = require('fs');
const argv = require('yargs').argv;
let directory = argv.xmlfolder ;
const xml2js = require('xml2js');

const parser = new xml2js.Parser({
    explicitArray: false,
    mergeAttrs: true,
    explicitRoot: false,
    ignoreAttrs: false,
});
let xml_string = "";
async function loadjs() {
    let folder = await fs.readdirSync(`${directory}`);

        for (let index in folder) {
            let path = `${directory}/${folder[index]}`;
            let stats = fs.statSync(path);
            if(stats.isDirectory()) {
                if(folder[index].includes("-in-")) {
                    let folder = await fs.readdirSync(path);
                    for (let index in folder) {
                        readFileContent(`${path}/${folder[index]}`);
                    }
                }
            }else {
                readFileContent(`${directory}/${folder[index]}`);
            }
    }
    xml_string = xml_string.replace(/^/, '<operations>');
    xml_string = xml_string.concat("</operations>");
    // xml_string = fs.readFileSync(`07-lot.xml`, "utf8");
    let newXmlAssigned =  await loadVars(xml_string);
    return newXmlAssigned;
}
async function readFileContent(file)
{
    let tmp = fs.readFileSync(file, "utf8");
    let filteredText = tmp.replace(/<!--[\s\S]*?-->/g, "");
    parser.parseString(tmp, async function (error, result) {
        if (!result.ignore) {
            filteredText = filteredText.replace('<operations>', '');
            filteredText = filteredText.replace('</operations>', '');
            xml_string += filteredText;
        }
    });
}

async function loadVars(xml_string)
{
    const buffer = fs.readFileSync(argv.params);
    let data = JSON.parse(buffer);
    for (var key in data) {
        xml_string = xml_string.replaceAll("${" + key + "}", data[key]);
    }
    return xml_string;
}
module.exports = {
    loadjs
}