/*jshint esversion: 8 */
const { By } = require('selenium-webdriver');

async function getLocator(object) {
    let inputLocator;
    if (object.getBy == "1") inputLocator = By.id(object.value);
    else if (object.getBy == "2") inputLocator = By.name(object.value);
    else if (object.getBy == "3") inputLocator = By.xpath(object.value);
    else if (object.getBy == "4") inputLocator = By.className(object.value);
    return inputLocator;
}
module.exports = {
    getLocator
}