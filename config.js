/**
 * @author Nadhem Med Rached
 * @email nrached@teklend.com
 */
/*jshint esversion: 8 */
const argv = require('yargs').argv;

module.exports = {
    folder: (argv.folder || "**").trim(),
    navigator: (argv.navigator && !Array.isArray(argv.navigator) && argv.navigator || "CHROME").trim().toUpperCase(),
    headless: (argv.headless || "").trim(),
    maximized: (argv.maximized || "").trim(),
    actions: {
    },
    browsers: {
        CHROME: 'chrome',
        FIREFOX: 'firefox',
        IE: 'ie',
        EDGE: 'MicrosoftEdge'
    },
    run_time: {
        mouseMoveDuration: 20000,
        elementLocatedTimeOut: 10000,
        elementIsNotVisibleTimeOut: 10000,
        implicitTimeOut: 30000,
        elementToBeClickable: 30000,
        sleep: {
            light: 2000,
            long: 5000
        }
    },
    screenSize: {
        width: 1366,
        height: 768
    }
};