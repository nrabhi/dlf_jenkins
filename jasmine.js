const Jasmine = require('jasmine');
const jasmine = new Jasmine();
const JasmineConsoleReporter = require('jasmine-console-reporter');
const { prepareDriver, closeCurrentDriver } = require('./factory');
jasmine.loadConfigFile('spec/support/jasmine.json');
var jasmineReporters = require('jasmine-reporters');


const reporter = new JasmineConsoleReporter({
    colors: 1,           // (0|false)|(1|true)|2
    cleanStack: 1,       // (0|false)|(1|true)|2|3
    verbosity: 4,        // (0|false)|1|2|(3|true)|4|Object
    listStyle: 'indent', // "flat"|"indent"
    timeUnit: 'ms',      // "ms"|"ns"|"s"
    timeThreshold: { ok: 500, warn: 1000, ouch: 3000 }, // Object|Number
    activity: true,
    emoji: true,         // boolean or emoji-map object
    beep: true,
});
 

jasmine.configureDefaultReporter({
    // The `timer` passed to the reporter will determine the mechanism for seeing how long the suite takes to run.
    timer: new jasmine.jasmine.Timer(),
    // The `print` function passed the reporter will be called to print its results.
    print: function() {
        process.stdout.write(arguments);
    },
    // `showColors` determines whether or not the reporter should use ANSI color codes.
    showColors: true
});
// initialize and execute
jasmine.env.clearReporters();
jasmine.addReporter(reporter);
var nunitReporter = new jasmineReporters.JUnitXmlReporter({
    savePath: './reports',
    consolidateAll: true,
    
  });
  jasmine.env.addReporter(nunitReporter)

jasmine.exitOnCompletion = false;
jasmine.jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999999;
prepareDriver().then(async (res)  => {
const result =  await jasmine.execute();

if (result.overallStatus === 'passed') {
    closeCurrentDriver().then(() => {
        process.exit();
    });
} else {
    console.log('At least one spec has failed');
}
});